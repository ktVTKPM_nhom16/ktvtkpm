pipeline {
    agent any

    tools {
        maven 'my-maven'
    }

    environment {
        POSTGRES_ORDER_IMAGE = 'postgres'
        POSTGRES_INVENTORY_IMAGE = 'postgres'
        MONGO_IMAGE = 'mongo:4.4.14-rc0-focal'
        MYSQL_IMAGE = 'mysql:5.7'
        KEYCLOAK_IMAGE = 'quay.io/keycloak/keycloak:24.0.4'
        ZOOKEEPER_IMAGE = 'confluentinc/cp-zookeeper:7.0.1'
        KAFKA_IMAGE = 'confluentinc/cp-kafka:7.0.1'
        ZIPKIN_IMAGE = 'openzipkin/zipkin'
        DISCOVERY_SERVER_IMAGE = 'manh20051151/discovery-server:latest'
        API_GATEWAY_IMAGE = 'manh20051151/api-gateway:latest'
        PRODUCT_SERVICE_IMAGE = 'manh20051151/product-service:latest'
        ORDER_SERVICE_IMAGE = 'manh20051151/order-service:latest'
        INVENTORY_SERVICE_IMAGE = 'manh20051151/inventory-vervice:latest'
        NOTIFICATION_SERVICE_IMAGE = 'manh20051151/notification-service:latest'
    }

    stages {
        stage('Setup Network') {
            steps {
                script {
                    sh 'docker network create dev || echo "Network already exists"'
                }
            }
        }

        stage('Deploy Postgres Order') {
            steps {
                script {
                    sh "docker run -d --name postgres-order --network dev -e POSTGRES_DB=order-service -e POSTGRES_USER=ptechie -e POSTGRES_PASSWORD=password -e PGDATA=/data/postgres -v ${WORKSPACE}/postgres-order:/data/postgres -p 5431:5431 ${POSTGRES_ORDER_IMAGE}"
                }
            }
        }

        stage('Deploy Postgres Inventory') {
            steps {
                script {
                    sh "docker run -d --name postgres-inventory --network dev -e POSTGRES_DB=inventory-service -e POSTGRES_USER=ptechie -e POSTGRES_PASSWORD=password -e PGDATA=/data/postgres -v ${WORKSPACE}/postgres-inventory:/data/postgres -p 5432:5432 ${POSTGRES_INVENTORY_IMAGE}"
                }
            }
        }

        stage('Deploy MongoDB') {
            steps {
                script {
                    sh "docker run -d --name mongo --network dev -v ${WORKSPACE}/mongo-data:/data/db -p 27017:27017 ${MONGO_IMAGE}"
                }
            }
        }

        stage('Deploy Keycloak MySQL') {
            steps {
                script {
                    sh "docker run -d --name keycloak-mysql --network dev -v ${WORKSPACE}/mysql_keycloak_data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=keycloak -e MYSQL_USER=keycloak -e MYSQL_PASSWORD=password ${MYSQL_IMAGE}"
                }
            }
        }

        stage('Deploy Keycloak') {
            steps {
                script {
                    sh "docker run -d --name keycloak --network dev -e DB_VENDOR=MYSQL -e DB_ADDR=keycloak-mysql -e DB_DATABASE=keycloak -e DB_USER=keycloak -e DB_PASSWORD=password -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin -v ${WORKSPACE}/realms:/opt/keycloak/data/import/ -p 8080:8080 ${KEYCLOAK_IMAGE} start-dev --import-realm"
                }
            }
        }

        stage('Deploy Zookeeper') {
            steps {
                script {
                    sh "docker run -d --name zookeeper --network dev -e ZOOKEEPER_CLIENT_PORT=2181 -e ZOOKEEPER_TICK_TIME=2000 ${ZOOKEEPER_IMAGE}"
                }
            }
        }

        stage('Deploy Kafka Broker') {
            steps {
                script {
                    sh "docker run -d --name broker --network dev -p 9092:9092 -e KAFKA_BROKER_ID=1 -e KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181 -e KAFKA_LISTENER_SECURITY_PROTOCOL_MAP=PLAINTEXT:PLAINTEXT,PLAINTEXT_INTERNAL:PLAINTEXT -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://localhost:9092,PLAINTEXT_INTERNAL://broker:29092 -e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1 -e KAFKA_TRANSACTION_STATE_LOG_MIN_ISR=1 -e KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR=1 ${KAFKA_IMAGE}"
                }
            }
        }

        stage('Deploy Zipkin') {
            steps {
                script {
                    sh "docker run -d --name zipkin --network dev -p 9411:9411 ${ZIPKIN_IMAGE}"
                }
            }
        }

        stage('Deploy Discovery Server') {
            steps {
                script {
                    sh "docker run -d --name discovery-server --network dev -p 8761:8761 -e SPRING_PROFILES_ACTIVE=docker ${DISCOVERY_SERVER_IMAGE}"
                }
            }
        }

        stage('Deploy API Gateway') {
            steps {
                script {
                    sh "docker run -d --name api-gateway --network dev -p 8181:8080 -e SPRING_PROFILES_ACTIVE=docker -e LOGGING_LEVEL_ORG_SPRINGFRAMEWORK_SECURITY=TRACE ${API_GATEWAY_IMAGE}"
                }
            }
        }

        stage('Deploy Product Service') {
            steps {
                script {
                    sh "docker run -d --name product-service --network dev -e SPRING_PROFILES_ACTIVE=docker ${PRODUCT_SERVICE_IMAGE}"
                }
            }
        }

        stage('Deploy Order Service') {
            steps {
                script {
                    sh "docker run -d --name order-service --network dev -e SPRING_PROFILES_ACTIVE=docker -e SPRING_DATASOURCE_URL=jdbc:postgresql://postgres-order:5432/order-service ${ORDER_SERVICE_IMAGE}"
                }
            }
        }

        stage('Deploy Inventory Service') {
            steps {
                script {
                    sh "docker run -d --name inventory-service --network dev -e SPRING_PROFILES_ACTIVE=docker -e SPRING_DATASOURCE_URL=jdbc:postgresql://postgres-inventory:5432/inventory-service ${INVENTORY_SERVICE_IMAGE}"
                }
            }
        }

        stage('Deploy Notification Service') {
            steps {
                script {
                    sh "docker run -d --name notification-service --network dev -e SPRING_PROFILES_ACTIVE=docker ${NOTIFICATION_SERVICE_IMAGE}"
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
